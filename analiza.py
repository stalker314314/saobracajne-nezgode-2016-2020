# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib as plt
from datetime import datetime


def dateparse(d,t):
    dt = d + " " + t
    return datetime.strptime(dt, '%d.%m.%Y %H:%M')

def dateparse_weather(d,t):
    dt = d + " " + t
    return datetime.strptime(dt, '%Y-%m-%d %H:%M')

nezgode_df = pd.read_csv('/home/branko/Документа/nezgode/nez-all.csv',
                         parse_dates={'date': ['datum', 'vreme']}, date_parser=dateparse)
categories = ['Sa mat.stetom', 'Sa povredjenim', 'Sa poginulim']
cat_labels=['Sa mat. štetom', 'Sa povređenim', 'Sa poginulim']

# Per years
per_year_df = nezgode_df.groupby([nezgode_df.date.dt.year, nezgode_df.tip]).size().unstack()
per_year_df.columns = pd.CategoricalIndex(per_year_df.columns.values, ordered=True, categories=categories)
per_year_df = per_year_df.sort_index(axis=1)
per_year_df.plot(kind='bar', stacked=True, color=['green', 'yellow', 'red'])

# Per months
nezgode_df.groupby(nezgode_df.date.dt.to_period("M"))['id'].count().plot(kind='bar')

# Per month
per_month_df = nezgode_df.groupby([nezgode_df.date.dt.month, nezgode_df.tip]).size().unstack()
per_month_df.columns = pd.CategoricalIndex(per_month_df.columns.values, ordered=True, categories=categories)
per_month_df = per_month_df.sort_index(axis=1)
ax=per_month_df.plot(kind='bar', stacked=True, color=['green', 'yellow', 'red'])
handles, _ = ax.get_legend_handles_labels()
ax.legend(handles, labels)

# Per weekday
per_weekday_df = nezgode_df.groupby([nezgode_df.date.dt.dayofweek, nezgode_df.tip])['id'].size().unstack()
per_weekday_df.columns = pd.CategoricalIndex(per_weekday_df.columns.values, ordered=True, categories=categories)
per_weekday_df = per_weekday_df.sort_index(axis=1)
ax = per_weekday_df.plot(kind='bar', stacked=True, color=['green', 'yellow', 'red'])
handles, _ = ax.get_legend_handles_labels()
ax.legend(handles, labels)
ax.set_xticklabels(['Ponedeljak','Utorak','Sreda','Četvrtak', 'Petak', 'Subota', 'Nedelja'])

per_weekday_pct = nezgode_df.groupby([nezgode_df.date.dt.dayofweek, nezgode_df.tip])['id'].size().unstack()
per_weekday_pct['Procenata poginulih'] = 100 * per_weekday_pct['Sa poginulim'] / (per_weekday_pct['Sa mat.stetom'] + per_weekday_pct['Sa poginulim'] + per_weekday_pct['Sa povredjenim'])
ax = per_weekday_pct['Procenata poginulih'].plot(kind='bar')
ax.set_xticklabels(['Ponedeljak','Utorak','Sreda','Četvrtak', 'Petak', 'Subota', 'Nedelja'])

# Per hour
per_hour_df = nezgode_df.groupby([nezgode_df.date.dt.hour, nezgode_df.tip])['id'].size().unstack()
per_hour_df.columns = pd.CategoricalIndex(per_hour_df.columns.values, ordered=True, categories=categories)
per_hour_df = per_hour_df.sort_index(axis=1)
ax = per_hour_df.plot(kind='bar', stacked=True, color=['green', 'yellow', 'red'])
handles, _ = ax.get_legend_handles_labels()
ax.legend(handles, labels)

# Per hour, samo poginuli
per_hour_df=nezgode_df.groupby([nezgode_df.date.dt.hour, nezgode_df.tip])['id'].size().unstack()
per_hour_df['poginuli_pct'] = 100 * per_hour_df['Sa poginulim'] / (
    per_hour_df['Sa mat.stetom'] + per_hour_df['Sa poginulim'] + per_hour_df['Sa povredjenim'])
per_hour_df['poginuli_pct'].plot(kind='bar', color='red')

# Procena stanovnistva na 01.01.2020. preuzeta sa http://opendata.stat.gov.rs/odata/
stanovnistvo = {
    'BEOGRAD': 1691919,
    'UŽICE': 168150,
    'POŽAREVAC': 166777,
    'NOVI SAD': 618578,
    'JAGODINA': 198646,
    'VALJEVO': 162887,
    'KIKINDA': 136183,
    'SOMBOR': 172110,
    'ZAJEČAR': 106917,
    'SMEDEREVO': 185950,
    'NOVI PAZAR': 164063, # Novi Pazar, Tutin i Sjenica
    'KRAGUJEVAC': 282309,
    'PANČEVO': 278356,
    'ŠABAC': 278943,
    'ČAČAK': 199330,
    'SREMSKA MITROVICA': 298099,
    'VRANJE': 196971,
    'KRUŠEVAC':222907,
    'KRALJEVO':140744,
    'BOR': 112085,
    'NIŠ': 361446,
    'LESKOVAC': 199946,
    'SUBOTICA': 178853,
    'PIROT': 84244,
    'PROKUPLJE': 83743,
    'PRIJEPOLJE': 73149, # Nova Varos, Priboj i Prijepolje
    'ZRENJANIN': 174732,
}
stanovnistvo_df = pd.DataFrame.from_dict(stanovnistvo, orient='index', columns=['stanovnika'])

# Po gradovima
gradovi_df=nezgode_df.groupby(nezgode_df.grad).size().to_frame('nezgoda').join(stanovnistvo_df)
gradovi_df['pct'] = 200 * gradovi_df['nezgoda'] / gradovi_df['stanovnika']
gradovi_df.sort_values('pct', ascending=False)
gradovi_df['pct'].plot(kind='bar')

# Tipovi i ucesnici
nezgode_df[['tip', 'ucesnici']].groupby(['tip', 'ucesnici']).size().to_frame('count').\
    sort_values('count', ascending=False)

# Beograd samo
beograd_df=nezgode_df[nezgode_df.grad=='BEOGRAD'].groupby([nezgode_df.opstina, nezgode_df.tip]).size().unstack()
beograd_df.columns=pd.CategoricalIndex(beograd_df.columns.values, ordered=True, categories=categories)
beograd_df=beograd_df.sort_index(axis=1)
beograd_df.sort_values(by='Sa mat.stetom', ascending=False).plot(kind='bar', stacked=True, color=['green', 'yellow', 'red'])

# Vremenske prilike - kisa
weather_df = pd.read_csv('/home/branko/Документа/nezgode/weather.csv',
                         parse_dates={'datum': ['date', 'time']}, date_parser=dateparse_weather)
nezgode_beograd_2017_2019 = nezgode_df[nezgode_df.grad=='BEOGRAD'].groupby(nezgode_df.date.dt.to_period("d"))['id'].count().to_frame('broj nezgoda dnevno')
rain_2017_2019 = weather_df.groupby(weather_df.datum.dt.to_period("d"))['rain'].sum().to_frame('padavina [mm]')
rain_nezgode = nezgode_beograd_2017_2019.loc['2017-01-01':'2019-12-31'].join(rain_2017_2019)
rain_nezgode.plot.scatter('broj nezgoda dnevno', 'padavina [mm]')

# Histogram kisa
rain_nezgode_hist = rain_nezgode.groupby(pd.cut(rain_nezgode['padavina [mm]'], np.linspace(0, 6, 7))).mean()['broj nezgoda dnevno'].to_frame().unstack()
labels=[str(x[1]) for x in rain_nezgode_hist.index]
ax=rain_nezgode_hist.plot(kind='bar')
ax.set_xticklabels(labels)

# Vremenske prilike - sneg
snow_2017_2019=weather_df.groupby(weather_df.datum.dt.to_period("d"))['snow'].sum().to_frame('sneg [cm]')
nezgode_beograd_2017_2019.loc['2017-01-01':'2019-12-31'].join(snow_2017_2019).plot.scatter('broj nezgoda dnevno', 'sneg [cm]')