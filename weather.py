import requests
from bs4 import BeautifulSoup
import os.path
import pickle
import datetime
import time
import csv

if __name__ == '__main__':
    if os.path.exists('state.pickle'):
        with open('state.pickle', 'rb') as file:
            data = pickle.load(file)
    else:
        data = {}
    current_date = datetime.date(2016, 1, 1)
    to_date = datetime.date(2020, 12, 31)
    while current_date <= to_date:
        current_date_str = current_date.strftime('%Y-%m-%d')
        print(f'Processing {current_date_str}')
        if current_date_str in data:
            current_date = current_date + datetime.timedelta(days=1)
            continue
        payload = {'display_date': current_date_str}
        response = requests.post('https://www.weather2umbrella.com/istorijski-podaci-beograd-srbija-sr', data=payload)
        soup = BeautifulSoup(response.text, 'html.parser')
        hours = soup.select("div.weather_per_hours > div.three_hour_wrapper > div.row > div.col-sm-16 > div.row")
        hours_data = []
        for hour in hours:
            time_id = hour.select('div.three_hours_time > p')[0].text
            desc = hour.select('div.hourly_weather_description > div')[0].text
            temp = float(hour.select('div.three_hours_temp > p')[0].text[:-1])
            temp = (temp - 32) / 1.8
            snow = hour.select('div.three_hours_snow > p')[0].text
            if snow == '':
                snow = 0.0
            else:
                assert snow[-2:] == 'cm'
                snow = float(snow[:-2])
            rain = hour.select('div.three_hours_precipitation > p')[0].text
            if rain == '':
                rain = 0.0
            else:
                assert rain[-2:] == 'mm'
                rain = float(rain[:-2])
            pressure = hour.select('div.three_hours_pressure > p')[0].text
            if pressure == '':
                pressure = None
            else:
                pressure = float(pressure[:-4])
            wind = hour.select('div.three_hours_wind > p')[0].text.strip()[:-3]
            if wind == '':
                wind = 0.0
            else:
                wind = 0.44704 * float(wind)
            visibility = hour.select('div.three_hours_visibility > p')[0].text
            if visibility == '':
                visibility = None
            else:
                visibility = float(visibility[:-1])
            hour_data = {'time': time_id, 'description': desc, 'temp': temp, 'snow': snow, 'rain': rain,
                         'pressure': pressure, 'wind': wind, 'visibility': visibility}
            print(hour_data)
            hours_data.append(hour_data)
        data[current_date_str] = hours_data
        with open('state.pickle', 'wb') as file:
            pickle.dump(data, file)
        time.sleep(5)
        current_date = current_date + datetime.timedelta(days=1)
    with open('weather.csv', 'w', encoding='utf-8', newline='') as csvfile:
        fieldnames = ['date', 'time', 'description', 'temp', 'snow', 'rain', 'pressure', 'wind', 'visibility']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for one_date in data.keys():
            for one_hour in data[one_date]:
                row = {'date': one_date}
                row.update(one_hour)
                row['temp'] = round(row['temp'], 1)
                row['snow'] = round(row['snow'], 1)
                row['rain'] = round(row['rain'], 1)
                row['pressure'] = None if row['pressure'] is None else round(row['pressure'], 1)
                row['wind'] = round(row['wind'], 1)
                row['visibility'] = None if row['visibility'] is None else int(round(row['visibility'], -2))
                writer.writerow(row)
